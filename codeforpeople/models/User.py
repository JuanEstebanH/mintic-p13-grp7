from django.db import models
from django.contrib.auth.models import AbstractBaseUser,PermissionsMixin,BaseUserManager
from django.utils.translation import gettext as _



class UserManager(BaseUserManager):
    def create_user(self, username, password=None):
        """
        Creates and saves a user with the given username and password.
        """
        if not username:
            raise ValueError('Users must have an username')
        User = self.model(username=username)
        User.set_password(password)
        User.save(using=self._db)
        return User

    def create_superuser(self, username, password):
        """
        Creates and saves a superuser with the given username and password.
        """
        User = self.create_user(
            username=username,
            password=password,
        )
        User.is_admin = True
        User.save(using=self._db)
        return User     

class User(AbstractBaseUser, PermissionsMixin):

    class Types(models.TextChoices):
        Desarrollador="Desarrollador","Desarrollador"
        Cliente="Cliente","Cliente"

    User_id = models.BigAutoField(primary_key=True)

    type = models.CharField(
        _("type") , max_length=50, choices=Types.choices , default=Types.Desarrollador
    )

    password = models.CharField('password', max_length = 256)
    username = models.CharField('Username', max_length = 15, unique=True)
    avatar = models.CharField('avatar', max_length = 100,blank=True)

    objects = UserManager()
    USERNAME_FIELD = 'username'