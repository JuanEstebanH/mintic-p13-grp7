from rest_framework import serializers
from codeforpeople.models import Desarrollador
from codeforpeople.models import Estado_Solicitud
from codeforpeople.models import User
from codeforpeople.serializers.estado_solicitudSerializer import estado_solicitudSerializer

class desarrolladorSerializer(serializers.ModelSerializer):
    #Estado_Solicitud=estado_solicitudSerializer()
    class Meta:
        model = Desarrollador
        fields = ['username_Desarrollador','dev_id', 'dev_password', 'dev_dni', 'dev_name', 'dev_lastname', 'dev_email','dev_phone','dev_experience','dev_tools','Desarrollador_password']

    def create(self, validated_data):
        #estado_solicitudData = validated_data.pop('Estado_Solicitud')
        DesarrolladorInstance = Desarrollador.objects.create(**validated_data)
        #Estado_Solicitud.objects.create(user=DesarrolladorInstance, **estado_solicitudData)
        return DesarrolladorInstance

    def to_representation(self, obj):
        desarrollador = Desarrollador.objects.get(dev_id=obj.dev_id)
        #estadoSolicitud=Estado_Solicitud.objects.get(desarrollador = obj.id)
        return{
            'username_Desarrollador':desarrollador.username_Desarrollador,
            'dev_id':desarrollador.dev_id,
            'dev_password':desarrollador.dev_password,
            'dev_dni':desarrollador.dev_dni,
            'dev_name':desarrollador.dev_name,
            'dev_lastname':desarrollador.dev_lastname,
            'dev_email':desarrollador.dev_email,
            'dev_phone':desarrollador.dev_phone,
            'dev_experience':desarrollador.dev_experience,
            'dev_tools':desarrollador.dev_tools,
            'Desarrollador_password':desarrollador.Desarrollador_password,
            'password':User.password,
            'username':User.username

        }    
