from rest_framework_simplejwt.views import (TokenObtainPairView,TokenRefreshView)
from django.urls import path
from codeforpeople import views
from codeforpeople.views.ClienteDetailsView import clienteDetailView
urlpatterns = [
    path('cliente/',views.clienteCreateView.as_view()),
    path('desarrollo/',views.desarrolladorCreateView.as_view()),
    path('login/desarrollador/',TokenObtainPairView.as_view()),
    path('login/cliente/', TokenObtainPairView.as_view()),
    path('login/desarrollador/<int:pk>/',views.desarroladorDetailView.as_view()),
    path('login/cliente/<int:pk>/',views.clienteDetailView.as_view()),
]
