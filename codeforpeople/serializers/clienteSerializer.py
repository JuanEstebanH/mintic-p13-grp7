from rest_framework import serializers
from codeforpeople.models import Cliente
from codeforpeople.models import User
from codeforpeople.serializers.userSerializer import UserSerializer
class ClienteSerializer(serializers.ModelSerializer):

    #usuario = UserSerializer()
    class Meta:
        model = Cliente
        fields = ['username_cliente','client_id','cliente_dni','cliente_name', 'cliente_lastname', 'cliente_email', 'cliente_phone','cliente_password','username','password']

    def create(self, validated_data):
        #usuario = validated_data.pop('usuario')
        ClienteInstance = Cliente.objects.create(**validated_data)
        #usuario.objects.create(Cliente=ClienteInstance, **usuario)
        return ClienteInstance

    def to_representation(self, obj):
        cliente = Cliente.objects.get(dev_id=obj.dev_id)
        #usuario=User.objects.get(usuario = obj.id)
        return{
            'username_cliente':Cliente.username_cliente,
            'client_id':cliente.client_id,
            'cliente_dni':cliente.cliente_dni,
            'cliente_name':cliente.cliente_name,
            'cliente_lastname':cliente.cliente_lastname,
            'cliente_email':cliente.cliente_email,
            'cliente_phone':cliente.cliente_phone,
            'cliente_pasword':Cliente.cliente_password,
            'password':User.password,
            'username':User.username

        }    
