

from rest_framework import serializers
from codeforpeople.models import Estado_Solicitud
from codeforpeople.models import Solicitud_Cliente
from codeforpeople.serializers.estado_solicitudSerializer import estado_solicitudSerializer


class Solicitud_ClienteSerializer(serializers.ModelSerializer):
    estadoSolicitud=estado_solicitudSerializer()
    class Meta:
        Model: Solicitud_Cliente
        fields=['solicitud_cliente_id','titulo_solicitud','descr_solicitud','tipo_lang_framework' ,'img_solicitud','estado_solicitud_id_forkey']

        def create(self, validated_data):
            estado_solicitudData = validated_data.pop('estado_solicitud')
            SolicitudInstance = Solicitud_Cliente.objects.create(**validated_data)
            Estado_Solicitud.objects.create(Desarrollador=SolicitudInstance, **estado_solicitudData)
            return SolicitudInstance

    def to_representation(self, obj):
        solicitud_Cliente = Solicitud_Cliente.objects.get(dev_id=obj.dev_id)
        estadoSolicitud=Estado_Solicitud.objects.get(solicitud_Cliente = obj.id)
        return{
            'solicitud_cliente_id':solicitud_Cliente.solicitud_cliente_id,
            'titulo_solicitud':solicitud_Cliente.titulo_solicitud,
            'descr_solicitud':solicitud_Cliente.descr_solicitud,
            'tipo_lang_framework':solicitud_Cliente.tipo_lang_framework,
            'estadoSolicitud':{
                'estado_solicitud_id':estadoSolicitud.estado_solicitud_id,
                'Estado':estadoSolicitud.Estado,
                'dev_id_forkey':estadoSolicitud.dev_id_forkey,
                'client_id_forkey':estadoSolicitud.client_id_forkey,
            }
        }
    

        