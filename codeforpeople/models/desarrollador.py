from django.db import models
from codeforpeople.models import User
from django.contrib.auth.hashers import make_password


class DesarrolladorManager(models.Manager):
    def get_queryset(sef,*args,**kwargs):
        return super().get_queryset(*args, **kwargs).filter(type=User.Types.Desarrollador)

class Desarrollador(User):
    
    class meta:
        proxy=True


    objects = DesarrolladorManager()
    username_Desarrollador = models.CharField('username_Desarrollador', max_length = 15, unique=True)
    dev_id = models.BigAutoField('dev_id', primary_key=True)
    dev_password = models.CharField('Dev_Password', max_length = 256,default=None)
    dev_dni = models.IntegerField(unique=True)
    dev_name = models.CharField( max_length = 20, blank=False, default=None)
    dev_lastname = models.CharField( max_length=20, blank=False, default=None)
    dev_email = models.EmailField('Email', max_length = 100)
    dev_phone = models.CharField( max_length = 20, blank=False, default=None)
    dev_experience = models.TextField(max_length=1000, help_text="Ingrese una breve descripción de su experiencia")
    dev_tools = models.TextField(max_length=1000, help_text="Que herramientas de desarrollo domina?")
    Desarrollador_password = models.CharField( 'Desarrollador_password', max_length=50, null=False,default="String")


    
    def save(self,*args,**kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN' 
        self.password = make_password(self.cliente_password, some_salt)

        if not self.username:
            self.username = self.username_Desarrollador

        if not self.pk:
            self.type = User.Types.Cliente
        super().save(*args,**kwargs)        
   

    