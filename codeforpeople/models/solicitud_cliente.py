from django.db import models


from codeforpeople.models import Estado_Solicitud

class Solicitud_Cliente(models.Model):
    solicitud_cliente_id = models.AutoField(primary_key=True)
    titulo_solicitud = models.CharField( max_length = 20, blank=False, default=None)
    descr_solicitud = models.TextField(max_length=500, help_text="Ingrese descripcion de su solicitud")
    tipo_lang_framework = models.CharField( max_length = 50, blank=False, default=None, help_text="Preferencia de lenguajes/framework que desea que sean usados")
    img_solicitud = models.ImageField(upload_to='Photos',help_text="carga una imagen que acompañe la propuesta")
    estado_solicitud_id_forkey = models.ForeignKey(
        Estado_Solicitud,
        on_delete=models.CASCADE,
        related_name='%(class)s_estado_solicitud_id',
        blank=True,
        null=True
    )