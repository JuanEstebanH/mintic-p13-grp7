from django.contrib import admin
from .models.cliente import Cliente
from .models.User import User
from .models.desarrollador import Desarrollador
from .models.estado_solicitud import Estado_Solicitud
from .models.solicitud_cliente import Solicitud_Cliente

admin.site.register(Cliente)
admin.site.register(User)
admin.site.register(Desarrollador)
admin.site.register(Estado_Solicitud)
admin.site.register(Solicitud_Cliente)

# Register your models here.
