from django.db import models
from codeforpeople.models import User
from django.core.validators import MaxValueValidator, MinValueValidator

from django.contrib.auth.hashers import make_password
class ClienteManager(models.Manager):
    def get_queryset(sef,*args,**kwargs):
        return super().get_queryset(*args, **kwargs).filter(type=User.Types.Cliente)
class Cliente(User):
    class meta:
        proxy=True

    objects = ClienteManager()

    username_cliente = models.CharField('username_cliente', max_length = 15, unique=True)
    client_id = models.BigAutoField(primary_key=True)
    cliente_dni = models.IntegerField(unique=True)
    cliente_name = models.CharField( 'Name',max_length = 20)
    cliente_lastname = models.CharField( 'Lastname', max_length=20)
    cliente_email = models.EmailField('Email', max_length = 100)
    cliente_password = models.CharField( 'cliente_password', max_length=50, null=False,default="String")

    cliente_phone = models.IntegerField(
        default=1,
        validators=[
            MaxValueValidator(99999999),
            MinValueValidator(10000000)
        ]
     )

    def save(self,*args,**kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN' 
        self.password = make_password(self.cliente_password, some_salt)

        if not self.username:
            self.username = self.username_cliente

        if not self.pk:
            self.type = User.Types.Cliente
        super().save(*args,**kwargs)        

   

