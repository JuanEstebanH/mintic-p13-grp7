from codeforpeople.models.estado_solicitud import Estado_Solicitud
from rest_framework import serializers

class estado_solicitudSerializer(serializers.ModelSerializer):
    class Meta:
        model= Estado_Solicitud
        fields=['estado_solicitud_id','solicitud_cliente_id','dev_id']

        
