from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from codeforpeople.serializers.solicitudCilienteSerializer import Solicitud_ClienteSerializer
class ordenCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = Solicitud_ClienteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        tokenData = {"dev_name":request.data["dev_name"],
        "dev_password":request.data["dev_password"]}
        tokenSerializer = TokenObtainPairSerializer(data=tokenData)
        tokenSerializer.is_valid(raise_exception=True)
        return Response(tokenSerializer.validated_data, status=status.HTTP_201_CREATED)