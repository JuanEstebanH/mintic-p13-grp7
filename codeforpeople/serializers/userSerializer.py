from rest_framework import serializers

from codeforpeople.models import User

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['User_id', 'type', 'password', 'username', 'avatar']

    def create(self, validated_data):
        userInstance = User.objects.create(**validated_data)
        return userInstance

