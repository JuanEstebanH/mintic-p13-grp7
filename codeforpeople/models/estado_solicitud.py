from django.db import models

from codeforpeople.models import Cliente
from codeforpeople.models import Desarrollador
#from codeforpeople.models import Solicitud_Cliente

class Estado_Solicitud(models.Model):
    
    estado_solicitud_id = models.AutoField(primary_key=True)
    """
    solicitud_cliente_id = models.ForeignKey(
        Solicitud_Cliente,
        on_delete=models.CASCADE,
        related_name='%(class)s_estado_solicitud_cliente',
        blank=False,
        null=False
    )
    """
    ESTADOS = [
        ('Aprobada', 'Aprobada'),
        ('No Aprobada', 'No Aprobada'),
    ]
    Estado = models.CharField(
        max_length=100,
        choices=ESTADOS,
        default='No seleccionado',
    )

    dev_id_forkey = models.ForeignKey(
        Desarrollador,
        on_delete=models.CASCADE,
        related_name='%(class)s_dev_id',
        blank=False,
        null=False
    )

    client_id_forkey = models.ForeignKey(
        Cliente,
        on_delete=models.CASCADE,
        related_name='%(class)s_client_id',
        blank=False,
        null=False
    )



    #dev_id_forkey = models.ForeignKey(Desarrollador, related_name='dev_id', on_delete=models.CASCADE,null=False,blank=True,default=None)
    #client_id_forkey = models.ForeignKey(Cliente, related_name='client_id', on_delete=models.CASCADE,null=False,blank=True,default=None)

